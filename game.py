import os, sys
import pygame
from pygame.locals import *

class HiraganaQuizMain:
    #initialzie screen
    pygame.init()
    screen = pygame.display.set_mode((600, 350))
    pygame.display.set_caption('Hiragana QUIZ')

    #Fill bg
    background = pygame.Surface(screen.get_size())
    background = background.convert()
    background.fill((250, 250, 250))

    #display image
    hiraganaImg = pygame.image.load('images/A.png')
    hiraganaImg = pygame.transform.scale(hiraganaImg, (200, 150))
    hiraganaImgPos = hiraganaImg.get_rect()
    hiraganaImgPos = hiraganaImgPos.move((200, 0))
    background.blit(hiraganaImg, hiraganaImgPos)

    #blit everythong to screen (render it to screen)
    screen.blit(background, (0, 0))
    pygame.display.flip()

    #event loop
    while 1:
            for event in pygame.event.get():
                    if event.type == QUIT:
                            print("EX")
                            sys.exit()
            
            screen.blit(background, (0, 0))
            pygame.display.flip()


if __name__ == '__main__':
    HiraganaQuizMain()